//
//  SignInView.swift
//  ws2019
//
//  Created by Admin on 09.02.2021.
//

import SwiftUI

struct SignInView: View {
    
    @State var content = "in"
    
    var body: some View {
        if content == "in" {
            InView(con: $content)
        } else {
            UpView(con: $content)
        }
    }
}

struct InView: View {
    @Binding var con : String
    @State var colorSF =  false
    @State var email = ""
    @State var pass = ""
    var body: some View {
        ZStack {
            Image("stepback").resizable().frame(width: screen.width, height: screen.height).scaledToFit()
            VStack(spacing: 26) {
                Image("logo").resizable().frame(width: screen.width/3, height: 90).scaledToFit()
                CustomTF(text: $email, placeholder: "Email")

                
                CustomST(text: $pass, placeholder: "Password")
                Button(action: {
                    con = "main"
                }, label: {
                    Text("Sign In").foregroundColor(.white)
                })
                .frame(width: 320 ,height: 50)
                .background(Color("green"))
                .cornerRadius(25)
                Button(action: {}, label: {
                    HStack(spacing: 3) {
                        Image("facebook").resizable().frame(width: 20, height: 20).scaledToFit()
                        Text("Sign in with Facebook").foregroundColor(.white)
                    }
                   
                })
                .frame(width: 320 ,height: 50)
                .background(Color("green"))
                .cornerRadius(25)
                Button(action: {
                    con = "up"
                }, label: {
                    Text("Sign up").foregroundColor(Color("green"))
                }).padding(.bottom)
                .frame(width: 320 ,height: 50)
            }
            .padding(.horizontal, 45)
            .frame(width: screen.width/3+60, height: screen.height/2+60, alignment: .center)
            .background(
                Rectangle()
                    .frame(width: screen.width/3+100, height: screen.height/2+60, alignment: .center)
                    .foregroundColor(Color("gray"))
                    .cornerRadius(20)
            )
        }

        .edgesIgnoringSafeArea(.all)
      
    }
}


struct UpView: View {
    
    @Binding var con : String
    @State var screenShow =  ""
    @State var colorTF =  ""
    @State var nick = ""
    @State var email = ""
    @State var pass = ""
    @State var code = ""
    @State var num  = ""
    @State var repass = ""
    var body: some View {
        ZStack {
            Image("stepback").resizable().frame(width: screen.width, height: screen.height).scaledToFit()
            if screenShow == "" {
                VStack(spacing: 25) {
                    Image("logo").resizable().frame(width: screen.width/3, height: 90).scaledToFit()
                    //email
                    CustomTF(text: $email, placeholder: "Email")
                    //name
                    CustomTF(text: $nick, placeholder: "Nickname")
                    HStack (spacing: 10){
                        VStack (spacing: 8) {
                            HStack (spacing: 0) {
                        Text("+").foregroundColor(.white)
                        VStack {
                            ZStack(alignment: .leading) {
                            Text("Code").foregroundColor(Color("lgray")).opacity(code == "" ? 1 : 0)
                                TextField("", text: $code, onCommit: {
                                    if colorTF == "1" {
                                        colorTF = ""
                                    }
                                })
                                .keyboardType(.numberPad)
                                .accentColor(Color("green"))
                            .foregroundColor(.white)
                                .onTapGesture {
                                    if colorTF == "" {
                                       colorTF = "1"
                                    }
                                }
                        }
                        }.frame(width: 50)
                            }
                        Rectangle().frame(width: 52,height: 1, alignment: .center)
                            .foregroundColor(colorTF == "1" ? Color("green") : Color("lgray"))
                        }
                        CustomTF(text: $num, placeholder: "Phone").keyboardType(.numberPad)
                    }
                    //pass
                    CustomST(text: $pass, placeholder: "Password")
                    //repass
                    CustomST(text: $repass, placeholder: "Repeat password")

                    // sign up button
                    Button(action: {
                        screenShow = "Code"
                    }, label: {
                        Text("Sign up").foregroundColor(.white)
                    })
                    .frame(width: 320 ,height: 50)
                    .background(Color("green"))
                    .cornerRadius(25)
                    // sign in button
                    Button(action: {
                        con = "in"
                    }, label: {
                        Text("Sign in").foregroundColor(Color("green"))
                    }).padding(.bottom)
                    .frame(width: 320 ,height: 50)
                }
                .padding(.horizontal, 45)
                .frame(width: screen.width/3+60, height: screen.height/2+60, alignment: .center)
                .background(
                    Rectangle()
                        .frame(width: screen.width/3+100, height: screen.height/1.5, alignment: .center)
                        .foregroundColor(Color("gray"))
                        .cornerRadius(20)
                )
            } else {
                CodeView(num: "+\(code) \(num)")
            }
         
        }
        .edgesIgnoringSafeArea(.all)
    }
}

struct CodeView: View {
    @State var code = ""
    var num: String
    var body: some View {
        VStack(spacing: 20) {
            Image("logo").resizable().frame(width: screen.width/3, height: 90).scaledToFit().padding(.top)
            VStack (spacing: 5) {
                Text("Activation code was sent to number:").foregroundColor(Color("lgray"))
                Text("\(num)").foregroundColor(Color("lgray"))
            }
            VStack {
                ZStack(alignment: .leading) {
                    TextField("", text: $code)
                        .accentColor(.clear)
                        .font(.custom("", size: 35))
                        .foregroundColor(.clear)
                    .frame(width: 120)
                    .keyboardType(.numberPad)
                        .onReceive(code.publisher) { _ in
                            if code.count > 4 {
                                code.removeLast()
                            }
                        }
                    Text("\(code)") .font(.custom("", size: 35)).foregroundColor(.white)
                        .tracking(12)
                        .offset(x: -5)
                }
             
                HStack (spacing: 8) {
                    ForEach(0..<4) { _ in
                        Rectangle()
                            .frame(width: 25, height: 1)
                            .foregroundColor(Color("lgray"))
                    }
                }
            }
            Text("299 sec").foregroundColor(Color("lgray"))
            Text("Send code again").foregroundColor(Color("lgray")).font(.custom("", size: 23))
                .padding(.bottom, 40)
        }
        
        .padding(.horizontal, 60)
        .frame(width: screen.width/3+60, height: screen.height/2+60, alignment: .center)
        .background(
            Rectangle()
                .frame(width: screen.width/3+100, height: screen.height/2-40, alignment: .center)
                .foregroundColor(Color("gray"))
                .cornerRadius(20)
        )
    }
}

struct CustomST: View {
    @Binding var text: String
    var placeholder : String
    @State var colorTF = ""
    var body: some View {
        VStack {
        ZStack(alignment: .leading) {
            Text("\(placeholder)").foregroundColor(Color("lgray")).opacity(text == "" ? 1 : 0)
            SecureField("", text: $text) {
                if colorTF == "1" {
                    colorTF = ""
                }
            }
            .onTapGesture {
                if colorTF == "" {
                   colorTF = "1"
                }
            }
            .accentColor(Color("green"))
            .foregroundColor(.white)
        }
            Rectangle().frame(height: 1, alignment: .center)
                .foregroundColor(colorTF == "1" ? Color("green") : Color("lgray"))
        }
    }
}

struct  CustomTF: View {
    @Binding var text: String
    var placeholder : String
    @State var colorTF = ""
    var body: some View {
        VStack {
            ZStack(alignment: .leading) {
            Text("\(placeholder)").foregroundColor(Color("lgray")).opacity(text == "" ? 1 : 0)
                TextField("", text: $text, onCommit: {
                    if colorTF == "1" {
                        colorTF = ""
                    }
                })

                .accentColor(Color("green"))
            .foregroundColor(.white)
                .onTapGesture {
                    if colorTF == "" {
                       colorTF = "1"
                    }
                }
        }
            Rectangle().frame(height: 1, alignment: .center)
                .foregroundColor(colorTF == "1" ? Color("green") : Color("lgray"))
        }
    }
}

struct SignInView_Previews: PreviewProvider {
    static var previews: some View {
        SignInView()
    }
}
let screen = UIScreen.main.bounds
