//
//  CustomAlert.swift
//  ws2019
//
//  Created by Admin on 10.02.2021.
//

import SwiftUI

struct CustomAlert: View {
    @Binding var show : Bool
    let screen = UIScreen.main.bounds
    @State var title : String
    @State var mess : String
    var body: some View {
      
        ZStack {
            
            VStack(alignment: .leading, spacing: 10) {
                Text("\(title)").font(.custom("Roboto", size: 40)).foregroundColor(title == "Success" ? Color("green") : Color("red")).padding(.top)
                Text("\(mess)").font(.custom("Roboto", size: 18)).foregroundColor(.white).lineLimit(5).multilineTextAlignment(.leading).frame(width: 350)
                HStack {
                    Spacer()
                Button {
                    show.toggle()
                } label: {
                    Text("OK").font(.custom("Roboto", size: 18)).foregroundColor(title == "Success" ? Color("green") : Color("red"))
                }.padding()
                }

            }
            .padding(.horizontal)
            .padding(.leading , 70)
            .background(Color("gray"))
            .frame(width: 450, height: 220)
            .cornerRadius(10)
            ZStack {
                Circle()
                    .stroke(title == "Success" ? Color("green") : Color("red"), lineWidth: 6)
                    .frame(width: 110)
                Image("\(title == "Success" ? "successAlert" : "erroeAlert")").resizable().frame(width:60 , height: 50).scaledToFit()
            }
            .frame(width: 140, height: 140)
            .background(Color("gray"))
            .cornerRadius(70)
            .offset(x:-220)
        }
        
        
       
  
    }
}

