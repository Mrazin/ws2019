//
//  MainView.swift
//  ws2019
//
//  Created by Admin on 10.02.2021.
//

import SwiftUI

struct MainView: View {
    
    @Binding var content : String
    var body: some View {
        ZStack {
            Image("stepback").resizable().frame(width: screen.width, height: screen.height).scaledToFit()
            
            HStack {
                LeftMenu()
                MainMenu()
            }
            
        }
        .edgesIgnoringSafeArea(.all)
    }
}

struct  LeftMenu: View  {
    var body: some View {
        VStack {
            
        }
    }
}
struct  MainMenu: View  {
    var body: some View {
        VStack {
            
        }
    }
}
