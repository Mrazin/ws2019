//
//  StartSteps.swift
//  ws2019
//
//  Created by Admin on 09.02.2021.
//

import SwiftUI




struct StartSteps: View {
    @Binding  var content : String
    let screen = UIScreen.main.bounds
    @State var isSelect = "1"
    var body: some View {
    
        ZStack {
            Image("stepback").resizable().frame(width: screen.width, height: screen.height).scaledToFit()
            Rectangle()
                .frame(width: screen.width-50, height: screen.height, alignment: .center)
                .foregroundColor(Color("gray").opacity(0.85))
                .blur(radius: 30)
        
            TabView(selection: $isSelect ) {
                
                StepView(content: $content, step: Step(title: "Select the quest!", description: "A huge collection of different quests. Historical, children's, outdoors and many others...", image: "step1", currentPage: "1")).tag("1")
                StepView(content: $content, step: Step(title: "Complete the task!", description: "Search for secret keys, location detection, step counting and much more...", image: "step2", currentPage: "2")).tag("2")
                StepView(content: $content, step: Step(title: "Become a Top Key Finder", description: "User ratings, quest ratings, quest author ratings...", image: "step3", currentPage: "3")).tag("3")
                
            }
            .tabViewStyle(PageTabViewStyle())
        }
        .frame(width: screen.width, height: screen.height)
        .edgesIgnoringSafeArea(.all)
        .onAppear(perform: {
            UIPageControl.appearance().currentPageIndicatorTintColor = UIColor(Color("green"))
            UIPageControl.appearance().pageIndicatorTintColor = UIColor(.white)
        })
    }
}



struct Step: Hashable {
    var title: String
    var description: String
    var image: String
    var currentPage: String
}



struct StepView: View {
    @Binding var content : String
    @State var step : Step
    var body: some View {
       
            VStack {
                Image("\(step.image)").resizable().frame(width: 300, height: 300, alignment: .center).padding(.top, 120)
                Text("\(step.title)").font(.custom("Roboto", size: 50)).bold()
                Text("\(step.description)").font(.custom("Roboto", size: 25)).lineLimit(2).padding().frame(width: 600).multilineTextAlignment(.center)
                Spacer()
                HStack {
                    
                    Button(action: {
                        content = "auth"
                    }, label: {
                        Text("Skip").foregroundColor(.white).font(.custom("Roboto", size: 25)).opacity(step.currentPage == "3" ? 0 : 1 )
                    })
                    Spacer()
                    Button(action: {
                        if step.currentPage == "3" {
                            content = "auth"
                        }
                    }, label: {
                        Text(step.currentPage == "3" ? "Done" : "Next").foregroundColor(Color("green")).font(.custom("Roboto", size: 25))
                    })
                    
                }.padding(.all, 30)
            }.foregroundColor(.white)
            .padding(.bottom)
    
    }
}


