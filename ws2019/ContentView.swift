//
//  ContentView.swift
//  ws2019
//
//  Created by Admin on 09.02.2021.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Home()
    }
}


struct Home: View {
    @State var contentStart = "step"
    @State var content = "main"
    var body: some View {
        if UserDefaults.standard.string(forKey: "content") == nil {
            switch contentStart  {
            case "step":
                StartSteps(content: $contentStart)
            case "auth":
                SignInView()
            case "main":
                MainView(content: $contentStart)
            default:
                StartSteps(content: $contentStart)
            }
        } else {
            switch content  {
            case "main":
                MainView(content: $content)
            case "auth":
                SignInView()
            default:
                MainView(content: $content)
            }
        }
        
    }
}





struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
